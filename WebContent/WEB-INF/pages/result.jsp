<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	${data.name }
	<br> Lon: ${data.coord.lon }
	<br> Lat: ${data.coord.lat }
	<br> Weather: ${data.weather[0].main }
	<br> Temperature: ${data.main.temp }
	<br>
</body>
</html>